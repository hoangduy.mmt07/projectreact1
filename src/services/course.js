import api from "../contants/api"

const CouserService = {
    get(){
        return api.get('/elearning/v4/courses')
    },
    getDetail(id){
        return api.get(`/elearning/v4/courses/${id}`)
    }
}

export default CouserService