import api from "../contants/api"

export const AuthService = {
    login (data) {
        return api.post('/login', data)
    },

    refreshToken (data) {
        return api.post('/refresh-token', data)
    }
}

export default AuthService