import api from "../contants/api"

const UserService = {
    getInfo(){
        return api.get('/user/get-info')
    }
}

export default UserService