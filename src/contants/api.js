import axios from "axios";
import AuthService from "../services/auth";
import { getToken, setToken } from "../utils/token";

const api = axios.create({
    baseURL: import.meta.env.VITE_HOST
})

api.interceptors.response.use((res) => {
    return res.data
}, async (error) => {
    if(error.response.data.error_code === 'TOKEN_EXPIRED'){
        let token = getToken()

        if(token){
            const res = await AuthService.refreshToken({
                refreshToken: token.refreshToken
            })

            if(res.data){
                token.accessToken = res.data.accessToken

                setToken(token)

                return api(error.response.config)
            }
        }
    }

    return error.response.data
})

api.interceptors.request.use((config) => {
    let token = getToken()

    if(token){
        config.headers.Authorization = `Bearer ${token.accessToken}`
    }

    return config
})

export default api