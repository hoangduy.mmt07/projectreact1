import React from 'react'
import { Outlet } from 'react-router-dom'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Sidebar from '../components/Sidebar'
import LoginModal from '../components/LoginModal'

export default function MainLayouts() {
  return (
    <>
      <Header />
      <Sidebar />
      <Outlet />
      <Footer />
      <LoginModal />
    </>
  )
}
