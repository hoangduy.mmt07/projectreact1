import { createContext, useState } from 'react'
import logo from './logo.svg'
import './App.css'
import { Routes, Route } from 'react-router-dom';
import Home from './pages/index';
import Register from './pages/register';
import { 
    HOME_PATH,
    REGISTER_PATH,
    CONTACT_PATH,
    COURSE_PATH,
    COURSE_DETAIL_PATH,
    PROFILE_PATH,
    PROFILE_COURSE_PATH,
    PROFILE_PAYMENT_PATH,
    PROFILE_PROJECT_PATH,
    PROFILE_COIN_PATH
  } from './contants/path'
import Course from './pages/course';
import CourseDetails from './pages/course/[slug][id]';
import Page404 from './pages/404';
import Contact from './pages/contact';
import Profile from './pages/profile';
import ProfileCourse from './pages/profile/course';
import Project from './pages/profile/project';
import Payment from './pages/profile/payment';
import Coin from './pages/profile/coin';
import MainLayouts from './layouts/MainLayouts';
import ProfileLayouts from './layouts/ProfileLayouts';
import { AuthProvider } from './hooks/useAuth';
import { PageProvider } from './hooks/usePage';
import { Provider } from 'react-redux';
import { store } from './stores';

function App() {
  return (
    <Provider store={store}>
      <AuthProvider>
        <PageProvider>
          <Routes>
            <Route element={<MainLayouts />}>
              <Route path={HOME_PATH} element={<Home />}></Route>
              <Route path={REGISTER_PATH} element={<Register />}></Route>
              <Route path={CONTACT_PATH} element={<Contact />}></Route>
              <Route path={COURSE_PATH}>
                <Route index element={<Course />}></Route>
                <Route path={COURSE_DETAIL_PATH} element={<CourseDetails />}></Route>
              </Route>
              <Route path={PROFILE_PATH} element={<ProfileLayouts />}>
                <Route index element={<Profile />}></Route>
                <Route path={PROFILE_COURSE_PATH} element={<ProfileCourse />}></Route>
                <Route path={PROFILE_PROJECT_PATH} element={<Project />}></Route>
                <Route path={PROFILE_PAYMENT_PATH} element={<Payment />}></Route>
                <Route path={PROFILE_COIN_PATH} element={<Coin />}></Route>
              </Route>
              <Route path='*' element={<Page404 />}></Route>
            </Route>
          </Routes>
        </PageProvider>
      </AuthProvider>
    </Provider>
  )
}

export default App
