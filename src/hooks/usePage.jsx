import { createContext, useContext, useState } from "react"

const PageContext = createContext({})

export const PageProvider = ({ children }) => {
    const [isOpenLoginModal, setIsOpenLoginModal] = useState(false)

    return <PageContext.Provider value={{ 
        isOpenLoginModal,
        setIsOpenLoginModal
    }}>{children}</PageContext.Provider>
}

export const usePage = () => useContext(PageContext)