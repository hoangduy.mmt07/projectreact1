import { useEffect, useState } from "react"
import CouserService from "../services/course"

export const useCourse = () => {
    let [courses, setCourses] = useState([])

    useEffect(() => {
        CouserService.get().then(res => {
            setCourses(res.data)
        })
    }, [])

    return courses
}