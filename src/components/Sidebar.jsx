import React from 'react'
import { Link, NavLink } from 'react-router-dom';
import { HOME_PATH, REGISTER_PATH, CONTACT_PATH, COURSE_PATH, COURSE_DETAIL_PATH } from '../contants/path'

export default function Sidebar() {
    const closeSidbar = () => {
        document.body.classList.remove('menu-is-show');
    }

    return (
        <>
            <nav className="nav">
            <ul>
                <li className="li_login">
                    <Link onClick={closeSidbar} to={REGISTER_PATH}>Đăng nhập</Link>
                    <Link onClick={closeSidbar} to={REGISTER_PATH}>Đăng ký</Link>
                </li>
                <li>
                    <Link onClick={closeSidbar} to={HOME_PATH}>Trang chủ</Link>
                </li>
                <li>
                    <Link onClick={closeSidbar} to={CONTACT_PATH}>CFD Team</Link>
                </li>
                <li>
                    <NavLink onClick={closeSidbar} to={COURSE_PATH}>Khóa Học</NavLink>
                </li>
                <li>
                    <Link onClick={closeSidbar} to={COURSE_PATH}>Dự Án</Link>
                </li>
                <li>
                    <NavLink onClick={closeSidbar} to={CONTACT_PATH}>Liên hệ</NavLink>
                </li>
            </ul>
            </nav>
            <div className="overlay_nav" onClick={closeSidbar} />
        </>
    )
}
