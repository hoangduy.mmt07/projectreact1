import React, { createContext, useContext } from 'react'
import { Link } from 'react-router-dom';
import { HOME_PATH, REGISTER_PATH, CONTACT_PATH, COURSE_PATH, COURSE_DETAIL_PATH, PROFILE_PATH } from '../contants/path'
import { useAuth } from '../hooks/useAuth';
import { usePage } from '../hooks/usePage';

export default function Header() {
  const showHideSidebar = (e) => {
    e.preventDefault();
    document.body.classList.toggle('menu-is-show');
  }

  const { user, onLogin, onLogout } = useAuth()
  const { isOpenLoginModal, setIsOpenLoginModal } = usePage()

  return (
    <header id="header">
      <div className="wrap">
        <div className="menu-hambeger" onClick={showHideSidebar}>
          <div className="button">
            <span />
            <span />
            <span />
          </div>
          <span className="text">menu</span>
        </div>
        <Link to={HOME_PATH} className="logo">
          <img src="./pulish/img/logo.svg" alt="" />
          <h1>CFD</h1>
        </Link>
        <div className="right">
          <div className="have-login">
              {
                user ? (
                  <div className="have-login">
                    <div className="account">
                      <a href="#" className="info">
                        <div className="name">{user.name}</div>
                        <div className="avatar">
                          <img src={user.avatar} alt="" />
                        </div>
                      </a>
                    </div>
                    <div className="hamberger">
                    </div>
                    <div className="sub">
                      <a href="#">Khóa học của tôi</a>
                      <Link to={PROFILE_PATH}>Thông tin tài khoản</Link>
                      <a href="javascript:void();" onClick={onLogout}>Đăng xuất</a>
                    </div>
                  </div>
                ) : (
                  <div className="not-login bg-none">
                      <a href="javascript:void()" className="btn-register" onClick={ev => setIsOpenLoginModal(true)}>Đăng nhập</a>
                      <a href="login.html" className="btn main btn-open-login">Đăng ký</a>
                  </div>
                )
              }
          </div>
        </div>
      </div>
    </header>
  )
}
