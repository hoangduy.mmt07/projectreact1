import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import { useAuth } from '../hooks/useAuth'
import { usePage } from '../hooks/usePage'
import AuthService from '../services/auth'
import UserService from '../services/user'

export default function LoginModal() {
    const { isOpenLoginModal, setIsOpenLoginModal } = usePage()
    const [value, setValue]= useState({})
    const [error, setError]= useState({})
    const [errorMessage, setErrorMessage] = useState('')
    const { setUser } = useAuth()

    const onSubmit = async (ev) => {
        ev.preventDefault()

        try {
            setErrorMessage('')
            const res = await AuthService.login(value)
            localStorage.setItem('login', JSON.stringify(res.data))

            const user = await UserService.getInfo()
            setUser(user.data)

            localStorage.setItem('user', JSON.stringify(user.data))
            setIsOpenLoginModal(false)

        } catch (error) {
            if (error.response.data.message) {
                setErrorMessage(error.response.data.message)
            }
        }
    }

    return ReactDOM.createPortal(
        <div className="popup-form popup-login" style={{display: isOpenLoginModal ? 'flex': 'none'}} onClick={ev => setIsOpenLoginModal(false)}>
            <div className="wrap" onClick={ev => ev.stopPropagation()}>
                <form className="ct_login" style={{display: 'block'}} onSubmit={onSubmit}>
                    <h2 className="title">Đăng nhập</h2>
                    {
                        errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>
                    }
                    <input type="text" placeholder="Email / Số điện thoại" onChange={(ev) => value.username = ev.target.value}/>
                    <input type="password" placeholder="Mật khẩu" onChange={(ev) => value.password = ev.target.value}/>
                    <button type="submit" className="btn rect main btn-login">đăng nhập</button>
                </form>
            </div>
        </div>,
        document.body
    )
}
