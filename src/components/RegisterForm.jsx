import React from 'react'
import { useState } from 'react'
import Input from './Input';
import Select from './Select';

export default function RegisterForm() {
    const [value, setValue] = useState({})
    const [error, setError] = useState({})
    const onSubmit = (ev) => {
        ev.preventDefault();

        const errorObj = {};

        if(!value?.name?.trim()){
          errorObj.name = 'Ten khong duoc de trong';
        }

        if(!value?.phone?.trim()){
          errorObj.phone = 'So dien thoai khong duoc de trong';
        } else if(!/(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(value.phone)){
          errorObj.phone = 'So dien thoai khong dung dinh dang'
        }

        if(!value?.email?.trim()){
          errorObj.email = 'Email khong duoc de trong';
        } else if(!/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(value.email)){
          errorObj.email = 'Email khong dung dinh dang';
        }

        setError(errorObj);

        if(Object.keys(errorObj).length === 0){
          alert('Valiate thanh cong');
        }
    }

  return (
    <>
        <main className="register-course" id="main">
          <section>
            <div className="container">
              <div className="wrap container">
                <div className="main-sub-title">ĐĂNG KÝ</div>
                <h1 className="main-title">Thực chiến front-end căn bản </h1>
                <div className="main-info">
                  <div className="date"><strong>Khai giảng:</strong> 15/11/2020</div>
                  <div className="time"><strong>Thời lượng:</strong> 18 buổi</div>
                  <div className="time"><strong>Học phí:</strong> 6.000.000 VND</div>
                </div>
                <form className="form" onSubmit={onSubmit}>
                    <Input
                        label="Ho va ten"
                        placeholder="Nhap ho va ten"
                        type="text"
                        onChange={ev => value.name = ev.target.value}
                        error={error.name}
                        required
                    />
                    <Input
                        label="So dien thoai"
                        placeholder="Nhap so dien thoai"
                        type="text"
                        error={error.phone}
                        onChange={ev => value.phone = ev.target.value}
                        required
                    />
                    <Input
                        label="Email"
                        placeholder="Nhap email"
                        type="text"
                        error={error.email}
                        onChange={ev => value.email = ev.target.value}
                        required
                    />
                    <Input
                        label="Facebook"
                        placeholder="Nhap facebook"
                        type="text"
                        onChange={ev => value.facebook = ev.target.value}
                    />
                    <Select 
                        label="Hinh thuc thanh toan"
                        options={[
                            "Chuyen khoan",
                            "Tien mat"
                        ]}
                        placeholder="Chon hinh thuc thanh toan"
                        onChange={(val) => value.payment = val}
                    />
                    <label>
                        <p>Ý kiến cá nhân</p>
                        <input type="text" placeholder="Mong muốn cá nhân và lịch bạn có thể học." />
                    </label>
                    <button type="submit" className="btn main rect">đăng ký</button>
                </form>
              </div>
            </div>
          </section>
          {/* <div class="register-success">
            <div class="contain">
                <div class="main-title">đăng ký thành công</div>
                <p>
                    <strong>Chào mừng Trần Nghĩa đã trở thành thành viên mới của CFD Team.</strong> <br>
                    Cảm ơn bạn đã đăng ký khóa học tại <strong>CFD</strong>, chúng tôi sẽ chủ động liên lạc với bạn thông qua facebook
                    hoặc số điện thoại của bạn.
                </p>
            </div>
            <a href="/" class="btn main rect">về trang chủ</a>
        </div> */}
        </main>
    </>
  )
}
