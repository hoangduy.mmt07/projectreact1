import React from 'react'
import styled from 'styled-components'

const ErrorMessage = styled.span`
  display: block;
  color: red;
  white-space: nowrap;
`

export default function Input({ label, placeholder, type, required, error, onChange }) {
  return (
    <label>
        <p>{label}{ required && <span>*</span>}</p>
        <div className='form-group'>
          <input onChange={onChange} type={type} placeholder={placeholder} />
          {error && <ErrorMessage>{error}</ErrorMessage>}
        </div>
    </label>
  )
}
