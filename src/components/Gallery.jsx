import React from 'react'
import Slider from 'react-slick'
import styled from 'styled-components'

const GalleryWrap = styled.div`
    .item{
        display: block;
        overflow: hidden;
        position: relative;

        img{
            position: absolute;
            top: 0;
            left: 0;
            opacity: 1!important;
            margin: 0!important;
            width: 100%!important;
            height: 100%!important;
            object-fit: cover;
        }

        &:before{
            content: '';
            display: block;
            padding-bottom: 100%;
        }
    }
`

const settings = {
    dots: true,
    arrow: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1
}

export default function Gallery() {
  return (
    <GalleryWrap>
        <Slider className="list" {...settings}>
            <div className="item">
                <img src="/pulish/img/img_team1.png" alt="" />
            </div>
            <div className="item">
                <img src="/pulish/img/img_team2.png" alt="" />
            </div>
            <div className="item">
                <img src="/pulish/img/img_team3.png" alt="" />
            </div>
            <div className="item">
                <img src="/pulish/img/img_team4.png" alt="" />
            </div>
            <div className="item">
                <img src="/pulish/img/img_team1.png" alt="" />
            </div>
            <div className="item">
                <img src="/pulish/img/img_team2.png" alt="" />
            </div>
            <div className="item">
                <img src="/pulish/img/img_team3.png" alt="" />
            </div>
            <div className="item">
                <img src="/pulish/img/img_team4.png" alt="" />
            </div>
        </Slider>
    </GalleryWrap>
  )
}
