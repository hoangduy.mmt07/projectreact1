import React from 'react'
import Slider from 'react-slick'
import styled from 'styled-components'

const TestimonialWrap = styled.div`
    .slick-slider{
        position: static;
    }

    .slick-arrow:before{
        display: none;
    }
`

function NextArrow(props) {
    const { onClick } = props
    return (
        <button
            type="button"
            onClick={onClick}
            className="slick-arrow slick-next btn_ctr next"
        ></button>
    )
  }
  
function PrevArrow(props) {
    const { onClick } = props
    return (
        <button
            type="button"
            onClick={onClick}
            className="slick-arrow slick-prev btn_ctr prev"
        ></button>
    )
  }

const settings = {
    dots: true,
    arrow: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />
};

export default function Testimonial() {
  return (
    <>
        <TestimonialWrap className="testimonial">
            <div className="testimonial-item">
                <div className="item">
                  <div className="text">
                    <div className="ct ct-1 active">
                      <div className="info">
                        <div className="name">
                          <h4>Tiến Tài</h4>
                          <p>Thành viên CFD1</p>
                        </div>
                        <div className="quotes"><img src="/pulish/img/quotes.svg" alt="" /></div>
                      </div>
                      <div className="content">
                        Mentor có tâm, tận tình. Mình tìm được hướng đi trong lập trình front-end qua
                        khóa học. Like cho 4 mentor.
                      </div>
                      <div className="bottom">
                        <a href="#" target="_blank"><img src="/pulish/img/facebook.svg" alt="" /></a>
                        <span>09/10/2020</span>
                      </div>
                    </div>
                    <div className="ct ct-2">
                      <div className="info">
                        <div className="name">
                          <h4>Phạm Thành Trung</h4>
                          <p>Thành viên CFD1</p>
                        </div>
                        <div className="quotes"><img src="/pulish/img/quotes.svg" alt="" /></div>
                      </div>
                      <div className="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin libero
                        id magna finibus, in maximus urna tincidunt. Nam at leo lacinia, interdum dolor
                        in, molestie lectus. Aenean porttitor purus at purus euismod tristique
                      </div>
                      <div className="bottom">
                        <a href="#" target="_blank"><img src="/pulish/img/facebook.svg" alt="" /></a>
                        <span>01/10/2020</span>
                      </div>
                    </div>
                    <div className="ct ct-3">
                      <div className="info">
                        <div className="name">
                          <h4>Nguyễn Văn Tuấn</h4>
                          <p>Thành viên CFD1</p>
                        </div>
                        <div className="quotes"><img src="/pulish/img/quotes.svg" alt="" /></div>
                      </div>
                      <div className="content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin libero
                        id magna finibus, in maximus urna tincidunt. Nam at leo lacinia, interdum dolor
                        in, molestie lectus. Aenean porttitor purus at purus euismod tristique
                      </div>
                      <div className="bottom">
                        <a href="#" target="_blank"><img src="/pulish/img/facebook.svg" alt="" /></a>
                        <span>01/10/2020</span>
                      </div>
                    </div>
                  </div>
                  <div className="images" style={{overflow: 'unset', position: 'static'}}>
                    <Slider className="list" {...settings}>
                      <div className="carousel-cell">
                        <div className="img">
                          <picture>
                            <source media="(max-width: 767px)" src="/pulish/img/Intersect.png" />
                            <img src="/pulish/img/tes.jpg" alt="" />
                          </picture>
                        </div>
                        <div className="ct_m">
                          <div className="info">
                            <div className="name">
                              <h4>Tiến Tài</h4>
                              <p>Thành viên CFD1</p>
                            </div>
                            <div className="quotes"><img src="/pulish/img/quotes.svg" alt="" /></div>
                          </div>
                          <div className="content">
                            Mentor có tâm, tận tình. Mình tìm được hướng đi trong lập trình
                            front-end qua
                            khóa học. Like cho 4 mentor.
                          </div>
                          <div className="bottom">
                            <a href="#" target="_blank"><img src="/pulish/img/facebook.svg" alt="" /></a>
                            <span>09/10/2020</span>
                          </div>
                        </div>
                      </div>
                      <div className="carousel-cell">
                        <div className="img">
                          <picture>
                            <source media="(max-width: 767px)" src="/pulish/img/Intersect.png" />
                            <img src="/pulish/img/tes.jpg" alt="" />
                          </picture>
                        </div>
                        <div className="ct_m">
                          <div className="info">
                            <div className="name">
                              <h4>Nguyễn Văn Tuấn</h4>
                              <p>Thành viên CFD1</p>
                            </div>
                            <div className="quotes"><img src="/pulish/img/quotes.svg" alt="" /></div>
                          </div>
                          <div className="content">
                            Mentor có tâm, tận tình. Mình tìm được hướng đi trong lập trình
                            front-end qua
                            khóa học. Like cho 4 mentor.
                          </div>
                          <div className="bottom">
                            <a href="#" target="_blank"><img src="/pulish/img/facebook.svg" alt="" /></a>
                            <span>09/10/2020</span>
                          </div>
                        </div>
                      </div>
                      <div className="carousel-cell">
                        <div className="img">
                          <picture>
                            <source media="(max-width: 767px)" src="/pulish/img/Intersect.png" />
                            <img src="/pulish/img/tes.jpg" alt="" />
                          </picture>
                        </div>
                        <div className="ct_m">
                          <div className="info">
                            <div className="name">
                              <h4>Phạm Thành Trung</h4>
                              <p>Thành viên CFD1</p>
                            </div>
                            <div className="quotes"><img src="/pulish/img/quotes.svg" alt="" /></div>
                          </div>
                          <div className="content">
                            Mentor có tâm, tận tình. Mình tìm được hướng đi trong lập trình
                            front-end qua
                            khóa học. Like cho 4 mentor.
                          </div>
                          <div className="bottom">
                            <a href="#" target="_blank"><img src="/pulish/img/facebook.svg" alt="" /></a>
                            <span>09/10/2020</span>
                          </div>
                        </div>
                      </div>
                    </Slider>
                  </div>
                </div>
            </div>
        </TestimonialWrap>
    </>
  )
}
