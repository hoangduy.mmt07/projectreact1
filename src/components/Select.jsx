import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import styled from 'styled-components'

const SelectWrap = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 30px;
    padding: 0 50px;

    p{
        width: 180px;
        font-family: ab;
    }

    &.isShow{
        .sub{
            display: block!important;
        }
    }
`

export default function Select({ label, options, placeholder, onChange }) {
    const [isShow, setIsShow] = useState(false);
    const [value, setValue] = useState(placeholder);

    const onClick = (ev) => {
        ev.stopPropagation();
        setIsShow(true);
    }

    const onSelect = (ev, value) => {
        ev.preventDefault();
        setValue(value);
        onChange?.(value);
    }

    useEffect(() => {
        const bodyClick = () => {
            setIsShow(false);
        }

        document.body.addEventListener('click', bodyClick);

        return () => {
            document.body.removeEventListener('click', bodyClick);
        }
    }, [])

    return (
        <SelectWrap className={isShow ? 'isShow': ''}>
            <p>{ label }</p>
            <div className="select">
            <div className="head" onClick={onClick}>{ value }</div>
            <div className="sub">
                {
                    options.map(e => <div key={e} onClick={(ev) => onSelect(ev, e)}>{e}</div>)
                }
            </div>
            </div>
        </SelectWrap>
    )
}
