import React from 'react'
import CourseCard from '../components/CourseCard'
import Gallery from '../components/Gallery'
import Testimonial from '../components/Testimonial'
import { useCourse } from '../hooks/useCourse'

export default function Home() {
  const courses = useCourse()
  return (
    <>
      <main className="homepage" id="main">
        <div className="banner jarallax">
          <div className="container">
            <div className="content">
              <h2 className="title">Thực Chiến</h2>
              <h2 className="title">Tạo ra sản phẩm có giá trị</h2>
              <div className="btn main round">KHÓA HỌC</div>
            </div>
          </div>
          <div className="jarallax-img">
            <video className="video-bg" autoPlay muted>
              <source src="/pulish/video/CFD-video-bg2.mp4" type="video/mp4"/>
              Your browser does not support the video tag.
            </video>
          </div>
        </div>
        <section className="section-courseoffline">
          <div className="container">
            <p className="top-des">
              The readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it
              has
              a more-or-less normal
            </p>
            <div className="textbox">
              <h2 className="main-title">Khóa học Offline</h2>
            </div>
            <div className="list row">
              {
                courses.map((course) => <CourseCard key={course.id} {...course}/>)
              }
            </div>
          </div>
        </section>
        <section className="section-courseonline section-blue">
          <div className="container">
            <div className="textbox">
              <h2 className="main-title">Khóa học Online</h2>
            </div>
            <div className="list row">
              <div className="col-md-4 course gray">
                <div className="wrap">
                  <a className="cover" href="#">
                    <img src="/pulish/img/img1.png" alt="" />
                    <div className="hover">
                      <div className="top">
                        <div className="user">
                          <img src="/pulish/img/icon-user-white.svg" alt="" />
                          12</div>
                        <div className="heart">
                          <img src="/pulish/img/icon-heart.svg" alt="" /> 100
                        </div>
                      </div>
                      <div className="share">
                        <img src="/pulish/img/icon-viewmore.svg" alt="" />
                      </div>
                    </div>
                  </a>
                  <div className="info">
                    <a href="#" className="name">
                      Front-end căn bản
                    </a>
                    <p className="des">
                      One of the best corporate fashion brands in Sydney
                    </p>
                  </div>
                  <div className="bottom">
                    <div className="teacher">
                      <div className="avatar">
                        <img src="/pulish/img/avt.png" alt="" />
                      </div>
                      <div className="name">Trần Nghĩa</div>
                    </div>
                    <div className="register-btn">Đăng Ký</div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 course gray">
                <div className="wrap">
                  <a className="cover" href="#">
                    <img src="/pulish/img/img1.png" alt="" />
                    <div className="hover">
                      <div className="top">
                        <div className="user">
                          <img src="/pulish/img/icon-user-white.svg" alt="" />
                          12</div>
                        <div className="heart">
                          <img src="/pulish/img/icon-heart.svg" alt="" /> 100
                        </div>
                      </div>
                      <div className="share">
                        <img src="/pulish/img/icon-viewmore.svg" alt="" />
                      </div>
                    </div>
                  </a>
                  <div className="info">
                    <a href="#" className="name">
                      Front-end nâng cao
                    </a>
                    <p className="des">
                      One of the best corporate fashion brands in Sydney
                    </p>
                  </div>
                  <div className="bottom">
                    <div className="teacher">
                      <div className="avatar">
                        <img src="/pulish/img/avt.png" alt="" />
                      </div>
                      <div className="name">Trần Nghĩa</div>
                    </div>
                    <div className="register-btn">Đăng Ký</div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 course gray">
                <div className="wrap">
                  <a className="cover" href="#">
                    <img src="/pulish/img/img1.png" alt="" />
                    <div className="hover">
                      <div className="top">
                        <div className="user">
                          <img src="/pulish/img/icon-user-white.svg" alt="" />
                          12</div>
                        <div className="heart">
                          <img src="/pulish/img/icon-heart.svg" alt="" /> 100
                        </div>
                      </div>
                      <div className="share">
                        <img src="/pulish/img/icon-viewmore.svg" alt="" />
                      </div>
                    </div>
                  </a>
                  <div className="info">
                    <a href="#" className="name">
                      Laravel framework
                    </a>
                    <p className="des">
                      One of the best corporate fashion brands in Sydney
                    </p>
                  </div>
                  <div className="bottom">
                    <div className="teacher">
                      <div className="avatar">
                        <img src="/pulish/img/avt.png" alt="" />
                      </div>
                      <div className="name">Trần Nghĩa</div>
                    </div>
                    <div className="register-btn">Đăng Ký</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="text-deco">C</div>
          </div>
        </section>
        <section className="section-different">
          <div className="container">
            <div className="row">
              <div className="titlebox col-md-6 col-sm-12 col-xs-12">
                <h2 className="main-title white textleft">Những điều <br /><span>đặc biệt</span> tại CFD</h2>
                <div className="videodif" data-src="video/CFD-video-intro.mp4">
                  <img src="/pulish/img/img-cfd-dac-biet.jpg" alt="" />
                  <div className="play-btn btn-video-intro">
                    <img src="/pulish/img/play-icon.svg" alt="" />
                  </div>
                </div>
              </div>
              <div className="contentbox col-md-6 col-sm-12 col-xs-12">
                <div className="item">
                  <h4>Không cam kết đầu ra</h4>
                  <p>Với CFD thì việc cam kết đầu ra nó sẽ không có ý nghĩa nếu như cả người hướng dẫn và
                    người
                    học không thật sự tâm huyết và cố gắng. Vì thế, đội ngũ CFD sẽ làm hết sức để giúp các
                    thành
                    viên tạo ra sản phẩm có giá trị, thay vì cam kết.
                  </p>
                </div>
                <div className="item">
                  <h4>Không chỉ là một lớp học</h4>
                  <p>CFD không phải một lớp học thuần túy, tất cả thành viên là một team, cùng hổ trợ, chia sẻ
                    và
                    giúp đỡ nhau trong suốt quá trình học và sau này, với sự hướng dẫn tận tâm của các thành
                    viên đồng sáng lập.
                  </p>
                </div>
                <div className="item">
                  <h4>Không để ai bị bỏ lại phía sau</h4>
                  <p>Vì chúng ta là một team, những thành viên tiếp thu chậm sẽ được đội ngũ CFD kèm cặp đặc
                    biệt,
                    cùng sự hổ trợ từ các thành viên khác. Vì mục tiêu cuối cùng là hoàn thành
                    khóa
                    học thật tốt.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="section-testimonial">
          <div className="container">
            <div className="textbox">
              <h2 className="main-title white">Học viên cảm nhận về CFD</h2>
            </div>
            <Testimonial />
          </div>
        </section>
        <section className="section-gallery">
          <div className="textbox">
            <h2 className="main-title">Chúng ta là một team</h2>
          </div>
          <Gallery />
          <div className="controls">
            <div className="btn_ctr prev" />
            <span>Trượt qua</span>
            <div className="timeline">
              <div className="process" />
            </div>
            <div className="btn_ctr next" />
          </div>
        </section>
        <section className="section-action">
          <div className="container">
            <h3>Bạn đã sẵn sàng trở thành chiến binh tiếp theo của Team CFD chưa?</h3>
            <div className="btn main round bg-white">Đăng ký</div>
          </div>
        </section>
      </main>
    </>
  )
}
