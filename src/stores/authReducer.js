let _user = localStorage.getItem('user')

if (_user) {
    _user = JSON.parse(_user)
}

const initState = {
    user: _user || null
}

export const authReducer = (state = initState, action) => {
    switch(action.type) {
        default: return state
        case 'auth/setUser':
            return { user: action.payload }
        case 'auth/logout':
            return { user: null }
    }
}