import { createStore } from "redux"
import { combineReducers } from "redux"
import { authReducer } from "./authReducer"

export const store = createStore(combineReducers({
    auth: authReducer
}),
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)